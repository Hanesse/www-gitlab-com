---
layout: markdown_page
title: Product categories
---

## Introduction

Below is the canonical list of product categories, grouped by DevOps lifecycle
stage and non-lifecycle groups, along with the name of the responsible product manager.

We want to an intuitive interfaces both within the company and with the wider community.
This it makes it more efficient for everyone to contribute or to get a question answered.
Therefore following interfaces are based on the product categories defined on this page:

- [Product Vision](https://about.gitlab.com/direction/product-vision/)
- [Direction](https://about.gitlab.com/direction/#functional-areas)
- [SDLC](https://about.gitlab.com/sdlc/#stacks)
- [Product Features](https://about.gitlab.com/features/)
- Our deck, the slides that we use to describe the company
- Engineering groups
- Product groups
- Product marketing specializations

![DevOps lifecycle](handbook/sales/devops-loop.svg)

The above DevOps lifecycle is the inspiration for the stages that fall inside that circle.
At GitLab the Dev and Ops split is different because our CI/CD functionality is one codebase that falls under Ops.

## Dev

- Product: [Job]
- Backend: n/a

### Inside the DevOps lifecycle

1. Plan - [Victor] and [James]
  - Chat integration - [Victor]
  - Issue Tracking - [Victor]
  - Issue Board - [Victor]
  - Portfolio Management - [Victor]
  - [Service Desk] - [Victor]
  - Conversational Development Index - [James]
  - Cycle Analytics - [James]
1. Create - [James] and [Victor]
  - Version Control - [James]
  - Code Review - [Victor]
  - Web IDE - [James]
  - [Geo] - [James]
  - Wiki - [James]
  - Gitaly - [James]

### Outside the DevOps lifecycle

1. Auth - [Jeremy]
  - Signup
  - User management & authentication (incl. LDAP)
  - Groups and [Subgroups]
  - Audit log
  - GitLab.com (subscriptions)
1. Edge - [Jeremy]
  - customers.gitlab.com
  - Internationalization
  - license.gitlab.com
  - version.gitlab.com
1. Gitter - n/a

## Ops

- Product: [Mark]
- Backend: n/a

### Inside the DevOps lifecycle

1. Verify - [Fabio]
  - [Continuous Integration (CI)]
  - Security Products (SAST, DAST, etc.)
  - GitLab Runner
1. Package - [Fabio]
  - Container Registry
  - Binary Repository
1. Release - [Fabio]
  - [Continuous Delivery (CD)] / Release Automation
  - [Pages]
1. Configure - [Fabio]
  - Application Control Panel
  - Infrastructure Configuration
  - Operations
  - Feature Flags
  - ChatOps
1. Monitor - [Josh]
  - Metrics
  - Tracing
  - Production monitoring
  - Error Tracking
  - Logging

### Outside the DevOps lifecycle

1. BizOps - [Josh]
1. Distribution - [Josh]
  - Omnibus
  - Cloud Native Installation

[Jeremy]: /handbook/product#jeremy-watson
[Fabio]: /handbook/product#fabio-busatto
[Josh]: /handbook/product#joshua-lambert
[Mark]: /handbook/product#mark-pundsack
[James]: /handbook/product#james-ramsay
[Job]: /handbook/product#job-van-der-voort
[Victor]: /handbook/product#Victor-wu
[Pages]: /features/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /features/service-desk/