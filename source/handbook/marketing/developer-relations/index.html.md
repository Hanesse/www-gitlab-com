---
layout: markdown_page
title: "Developer Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Welcome to the Developer Relations Handbook  

The Developer Relations organization includes Developer Advocacy and Community Advocacy.

Developer Relations Handbooks:  

- [Developer Advocacy](/handbook/marketing/developer-relations/developer-advocacy/)
- [Community Advocacy](/handbook/marketing/developer-relations/community-advocacy/)  
